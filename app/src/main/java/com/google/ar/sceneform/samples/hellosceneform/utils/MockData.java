package com.google.ar.sceneform.samples.hellosceneform.utils;

import android.os.Handler;
import android.util.Log;

import com.google.ar.sceneform.samples.hellosceneform.model.TrackedLocationObject;
import com.google.ar.sceneform.samples.hellosceneform.model.TrackedLocationUpdateCallback;


public class MockData {

    private final static String TAG = MockData.class.getSimpleName();

    int increment = 1;
    int currentIndex = 0;
    boolean start = false;
    boolean bounce = false;
    int trackedUpdateInterval = 10000; //millisecond
    TrackedLocationUpdateCallback mTrackedLocationUpdateCallBack;

    private Handler handler;

    public MockData(TrackedLocationUpdateCallback trackedLocationUpdateCallBack){
        this.mTrackedLocationUpdateCallBack = trackedLocationUpdateCallBack;

    }

    public void startMockData() {
        Log.d(TAG, "startMockData");
        start = true;
        runData();
    }

    public void stopMockData() {
        Log.d(TAG, "stopMockData");
        start = false;
    }


    private void runData() {
        Log.d(TAG, "start Runnable For Mock target CarLocation Update");

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                try {
                    if(start){
                        TrackedLocationObject locationObject = dataList[currentIndex];

                        if(bounce){
                            if( currentIndex >= (dataList.length - 1)){
                                increment = -1;
                            }else if( currentIndex == 0){
                                increment = 1;
                            }
                        }else{
                            if( currentIndex >= (dataList.length - 1)){
                                currentIndex = -1;
                            }
                        }

                        currentIndex += increment;
                        mTrackedLocationUpdateCallBack.OnNewTargetLocationUpdate(locationObject);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, "Error: " + e.getMessage());
                }

                if (start)
                    handler.postDelayed(this, trackedUpdateInterval);
            }
        }, 10000);

    }


    //location list

    TrackedLocationObject[] dataListssss = new TrackedLocationObject[]{

            new TrackedLocationObject(52.0527276992493, -0.8874621860242682, 90.0),
            new TrackedLocationObject(52.05276020390771, -0.8874637455540935, 95.0),
            new TrackedLocationObject(52.05279232953819, -0.8874623342755794, 100.0),
            new TrackedLocationObject(52.05283386372135, -0.8874641704152197, 105.0),
            new TrackedLocationObject(52.0528709368749, -0.887461230919403, 110.0),
            new TrackedLocationObject(52.05290662214121, -0.887458149656728, 115.0),
            new TrackedLocationObject(52.05294585683129, -0.8874519607980724, 120.0),
            new TrackedLocationObject(52.05299127710931, -0.8874427095844872, 130.0),
            new TrackedLocationObject(52.05303106877782, -0.887436198402034, 140.0),
            new TrackedLocationObject(52.05307125050192, -0.8874328638947238, 150.0),
            new TrackedLocationObject(52.05311171264146, -0.887421508514088, 160.0),
            new TrackedLocationObject(52.05314922824055, -0.887400023232664, 140.0),
            new TrackedLocationObject(52.05318267087333, -0.8873616306721932, 120.0),
            new TrackedLocationObject(52.05321416614384, -0.8873258293163233, 100.0),

            //turn around
            new TrackedLocationObject(52.05321416614384, -0.8873258293163233, 90.0),
            new TrackedLocationObject(52.05321416614384, -0.8873258293163233, 90.0),
            new TrackedLocationObject(52.05318267087333, -0.8873616306721932, 90.0),
            new TrackedLocationObject(52.05314922824055, -0.887400023232664, 90.0),
            new TrackedLocationObject(52.05311171264146, -0.887421508514088, 90.0),
            new TrackedLocationObject(52.05307125050192, -0.8874328638947238, 90.0),
            new TrackedLocationObject(52.05303106877782, -0.887436198402034, 90.0),
            new TrackedLocationObject(52.05299127710931, -0.8874427095844872, 90.0),
            new TrackedLocationObject(52.05294585683129, -0.8874519607980724, 90.0),
            new TrackedLocationObject(52.05290662214121, -0.887458149656728, 90.0),
            new TrackedLocationObject(52.0528709368749, -0.887461230919403, 90.0),
            new TrackedLocationObject(52.05283386372135, -0.8874641704152197, 90.0),
            new TrackedLocationObject(52.05279232953819, -0.8874623342755794, 90.0),
            new TrackedLocationObject(52.05276020390771, -0.8874637455540935, 90.0),
            new TrackedLocationObject(52.0527276992493, -0.8874621860242682, 90.0)
    };

    TrackedLocationObject[] singlePath = new TrackedLocationObject[]{

            new TrackedLocationObject(52.0527276992493, 0.8874621860242682),
            new TrackedLocationObject(52.05276020390771, -0.8874637455540935),
            new TrackedLocationObject(52.05279232953819, -0.8874623342755794),
            new TrackedLocationObject(52.05283386372135, -0.8874641704152197),
            new TrackedLocationObject(52.0528709368749, -0.887461230919403),
            new TrackedLocationObject(52.05290662214121, -0.887458149656728),
            new TrackedLocationObject(52.05294585683129, -0.8874519607980724),
            new TrackedLocationObject(52.05299127710931, -0.8874427095844872),
            new TrackedLocationObject(52.05303106877782, -0.887436198402034),
            new TrackedLocationObject(52.05307125050192, -0.8874328638947238),
            new TrackedLocationObject(52.05311171264146, -0.887421508514088),
            new TrackedLocationObject(52.05314922824055, -0.887400023232664),
            new TrackedLocationObject(52.05318267087333, -0.8873616306721932),
            new TrackedLocationObject(52.05321416614384, -0.8873258293163233)
    };


    TrackedLocationObject[] dataList = new TrackedLocationObject[]{

            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),
            new TrackedLocationObject(51.509768, -0.134811, 150.0),

    };


}