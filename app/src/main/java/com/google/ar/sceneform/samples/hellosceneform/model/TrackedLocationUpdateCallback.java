package com.google.ar.sceneform.samples.hellosceneform.model;

public interface TrackedLocationUpdateCallback {

    void OnNewTargetLocationUpdate(TrackedLocationObject locationObject);

}
