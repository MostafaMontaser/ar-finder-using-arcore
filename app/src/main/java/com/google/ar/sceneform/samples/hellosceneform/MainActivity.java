package com.google.ar.sceneform.samples.hellosceneform;

import android.Manifest;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.ar.core.Anchor;
import com.google.ar.core.Pose;
import com.google.ar.core.Session;
import com.google.ar.core.TrackingState;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.ViewRenderable;
import com.google.ar.sceneform.samples.hellosceneform.model.TrackedLocationObject;
import com.google.ar.sceneform.samples.hellosceneform.model.TrackedLocationUpdateCallback;
import com.google.ar.sceneform.samples.hellosceneform.utils.ARConfig;
import com.google.ar.sceneform.samples.hellosceneform.utils.MockData;
import com.google.ar.sceneform.samples.hellosceneform.utils.Util;
import com.google.ar.sceneform.ux.ArFragment;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements TrackedLocationUpdateCallback, SensorEventListener {
    private static final String TAG = "OCVSample::Activity";
    ImageView imageview;
    Bitmap icon;
    View targetView;
    View driverNudge;
    FrameLayout arrows;
    private ArFragment arFragment;
    private ViewRenderable testViewRenderable;
    private ViewRenderable arrowsRenderable;

    private static final double MIN_OPENGL_VERSION = 3.0;

    LinearLayout animatedLayout;
    ImageView img1;
    ImageView img2;
    ImageView img3;
    ImageView img4;
    ImageView img5;
    ImageView[] imageViews;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private static final int REQUEST_PERMISSIONS_LOCATION_SETTINGS_REQUEST_CODE = 33;


    private FusedLocationProviderClient mFusedLocationClient;

    protected static long MIN_UPDATE_INTERVAL = 30 * 1000; // 1 minute is the minimum Android recommends, but we use 30 seconds


    int counter = 0;
    MockData mockData;
    Location myLocation = null;
    TrackedLocationObject locationObject = null;
    boolean camSetLocation = false;
    LocationRequest locationRequest;
    private LocationCallback mLocationCallback;

    private float mDistance;
    private float mInitialBearing;
    private float mFinalBearing;

    private long lastUpdate = 0;
    private long lastArrowAnimated = 0;
    private long lastHeading = 0;
    private long lastTarget = 0;

    private float last_x, last_y, last_z;
    double last_degree;
    private static final int SHAKE_THRESHOLD = 600;
    private SensorManager sensorManager;
    private Sensor senAccelerometer;
    private Sensor senMagnet;
    private ImageView arrow;

    Node pointerNode = null;
    ArrayList<Node> nodes = new ArrayList<>();
    int arrowAnimationValue = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!checkIsSupportedDeviceOrFinish(this)) {
            return;
        }
        targetView = getLayoutInflater().inflate(R.layout.target_custom, null);
        imageview = (ImageView) targetView.findViewById(R.id.animated);
        arFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.ux_fragment);
        driverNudge = findViewById(R.id.driver_nudge);
        animatedLayout = (LinearLayout) targetView.findViewById(R.id.animated_view);
        arrow = new ImageView(getApplicationContext());
        arrow.setScaleType(ImageView.ScaleType.FIT_CENTER);
        arrow.setImageResource(R.drawable.arrow);
        arrow.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        imageViews = new ImageView[5];
        imageViews[0] = (ImageView) findViewById(R.id.img1);
        imageViews[1] = (ImageView) findViewById(R.id.img2);
        imageViews[2] = (ImageView) findViewById(R.id.img3);
        imageViews[3] = (ImageView) findViewById(R.id.img4);
        imageViews[4] = (ImageView) findViewById(R.id.img5);


        android.location.LocationManager lm = (android.location.LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {

                if (locationResult != null) {
                    myLocation = locationResult.getLastLocation();
                    String result = "requestLocationUpdates: Current Location Latitude is " +
                            myLocation.getLatitude() + "\n" +
                            "Current location Longitude is " + myLocation.getLongitude();

                    Log.d(TAG, "result: " + result);

                    onMyLocationUpdated(myLocation);


                } else
                    Log.d(TAG, "locationResult is null in mLocationCallback.onLocationResult");

            }
        };

    }

    Thread animateLeftThread;

    public synchronized void animateLeftArrows(final int n) {
        for (int i = 0; i < 5; i++) {
            imageViews[i].clearAnimation();
        }
        final int size;
        if (n <= 3)
            size = 3;
        else
            size = n;
        int count = 0;
        for (int i = size - 1; i >= 0; i--) {
            if (count < n) {
                imageViews[i].setImageResource(R.drawable.left_arrow);
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) imageViews[i].getLayoutParams();
                layoutParams.height = 110;
                imageViews[i].setLayoutParams(layoutParams);
            }
            count++;
        }
        animateLeftThread = new Thread(new Runnable() {
            @Override
            public void run() {
                int count = 0;
                for (int i = size - 1; i >= 0; i--) {
                    final int position = i;
                    final Animation increase = new ScaleAnimation(
                            1f, 1f, // Start and end values for the X axis scaling
                            1, 1.8f, // Start and end values for the Y axis scaling
                            Animation.RELATIVE_TO_SELF, 0f, // Pivot point of X scaling
                            Animation.RELATIVE_TO_PARENT, .3f); // Pivot point of Y scaling
                    if (count < n) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imageViews[position].startAnimation(increase);
                            }
                        });
                    }
                    count++;
                    // increase.setRepeatCount(Animation.INFINITE);
                    increase.setDuration(150);
                    try {
                        Thread.sleep(150);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                }
            }
        });
        animateLeftThread.start();


    }

    Thread animateRightThread;

    public synchronized void animateRightArrows(final int n) {
        for (int i = 0; i < 5; i++) {
            imageViews[i].clearAnimation();
        }
        final int init;
        if (n <= 3)
            init = 2;
        else
            init = 0;
        for (int i = init; i < init + n; i++) {
            imageViews[i].setImageResource(R.drawable.right_arrow);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) imageViews[i].getLayoutParams();
            layoutParams.height = 110;
            imageViews[i].setLayoutParams(layoutParams);
        }
        animateRightThread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = init; i < init + n; i++) {
                    final int position = i;
                    final Animation increase = new ScaleAnimation(
                            1f, 1f, // Start and end values for the X axis scaling
                            1, 1.8f, // Start and end values for the Y axis scaling
                            Animation.RELATIVE_TO_SELF, 0f, // Pivot point of X scaling
                            Animation.RELATIVE_TO_PARENT, .3f); // Pivot point of Y scaling
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            imageViews[position].startAnimation(increase);
                        }
                    });
                    // increase.setRepeatCount(Animation.INFINITE);
                    increase.setDuration(150);
                    try {
                        Thread.sleep(150);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                }
            }
        });
        animateRightThread.start();


    }

    public void showDots() {
        for (int i = 0; i < 5; i++) {
            imageViews[i].setImageResource(R.drawable.dot);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) imageViews[i].getLayoutParams();
            layoutParams.height = 60;
            imageViews[i].setLayoutParams(layoutParams);
            imageViews[i].clearAnimation();
        }
    }

    private void checkCameraPermission() {
        // Here, thisActivity is the current activity
        if (!Util.isPermissionsGranted(this)) {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION},
                    100);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.

        } else {
            createTargetView();
            arFind();
        }

    }

    private void createTargetView() {
        RotateAnimation rotateAnimation1 = new RotateAnimation(0, 360,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation1.setInterpolator(new LinearInterpolator());
        rotateAnimation1.setDuration(1000);
        rotateAnimation1.setRepeatCount(Animation.INFINITE);
        imageview.startAnimation(rotateAnimation1);
//
//        final ArrayList<ImageView> imageViews = new ArrayList<>();
//        for (int i = 0; i < 25; i++) {
//
//            final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, 0);
//            //layoutParams.bottomMargin=9*i;
//            final ImageView imageView = new ImageView(getApplicationContext());
//            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
//            imageView.setImageResource(R.drawable.arrow);
//            imageView.setLayoutParams(layoutParams);
//            animatedLayout.addView(imageView);
//            imageViews.add(imageView);
//        }
//
//
//        final ValueAnimator animator = ValueAnimator.ofFloat(0.0f, 1.0f);
//        animator.setRepeatCount(ValueAnimator.INFINITE);
//        animator.setDuration(1000L);
//        animator.setInterpolator(new LinearInterpolator());
//        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator animation) {
//
//                for (int i = 0; i < imageViews.size(); i++) {
//                    final float progress = (float) animation.getAnimatedValue();
//                    final float width = imageViews.get(i).getHeight();
//                    final float translationX = (width) * -1 * progress;
//                    imageViews.get(i).setTranslationY(translationX);
//                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) imageViews.get(i).getLayoutParams();
//                    layoutParams.width = (int) (100 + (imageViews.get(i).getY()));
//                    layoutParams.height = (int) (70 + ((imageViews.get(i).getY()) * .2));
//
//                    imageViews.get(i).setLayoutParams(layoutParams);
//                }
//            }
//        });
//        animator.start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (Util.isPermissionsGranted(this)) {
            createTargetView();
            arFind();
        } else {
            Toast.makeText(getApplicationContext(), "you must enable camera permission", Toast.LENGTH_SHORT);
            finish();
        }


    }

    @Override
    public void onPause() {
        super.onPause();
        if (sensorManager != null)
            sensorManager.unregisterListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        checkCameraPermission();

    }


    public void onStop() {
        super.onStop();
        stopLocationUpdates();
        if (mockData != null)
            mockData.stopMockData();
        if (sensorManager != null)
            sensorManager.unregisterListener(this);
    }

    boolean targetShown = false;

    private void showTarget() {
        if (!targetShown) {
            ViewRenderable.builder()
                    .setView(this, targetView)
                    .build()
                    .thenAccept(renderable -> {
                        testViewRenderable = renderable;
                    });

            ViewRenderable.builder()
                    .setView(this, arrow)
                    .build()
                    .thenAccept(renderable -> {
                        arrowsRenderable = renderable;
                    });

//        ModelRenderable.builder()
//                .setSource(this, R.raw.andy)
//                .build()
//                .thenAccept(renderable -> {
//                    andyRenderable = renderable;
//                })
//                .exceptionally(
//                        throwable -> {
//                            Toast toast =
//                                    Toast.makeText(this, "Unable to load andy renderable", Toast.LENGTH_LONG);
//                            toast.setGravity(Gravity.CENTER, 0, 0);
//                            toast.show();
//                            return null;
//                        });
            arFragment.getArSceneView().getScene().addOnUpdateListener(this::onSceneUpdate);

        }
        targetShown = true;

    }

    AnchorNode mAnchorNode;

    private void onSceneUpdate(FrameTime frameTime) {
        // Let the fragment update its state first.
        arFragment.onUpdate(frameTime);
        // If there is no frame then don't process anything.
        if (arFragment.getArSceneView().getArFrame() == null) {
            return;
        }

        // If ARCore is not tracking yet, then don't process anything.
        if (arFragment.getArSceneView().getArFrame().getCamera().getTrackingState() != TrackingState.TRACKING) {
            return;
        }

        if (mAnchorNode == null && testViewRenderable != null) {
            Session session = arFragment.getArSceneView().getSession();

            /*float[] position = {0, 0, -1};
            float[] rotation = {0, 0, 0, 1};
            Anchor anchor = session.createAnchor(new Pose(position, rotation));*/

            Vector3 cameraPos = arFragment.getArSceneView().getScene().getCamera().getWorldPosition();
            Vector3 cameraForward = arFragment.getArSceneView().getScene().getCamera().getForward();
            Vector3 position = Vector3.add(cameraPos, cameraForward.scaled(1.0f));


            // Create an ARCore Anchor at the position.
            Pose pose = Pose.makeTranslation(cameraPos.x, cameraPos.y, cameraPos.z);

            Anchor anchor = arFragment.getArSceneView().getSession().createAnchor(pose);

            mAnchorNode = new AnchorNode(anchor);
            mAnchorNode.setParent(arFragment.getArSceneView().getScene());


            pointerNode = new Node();
            pointerNode.setRenderable(testViewRenderable);
            pointerNode.setParent(mAnchorNode);
            pointerNode.setLocalPosition(new Vector3(0, 0 - .5f, 4));

            //-----------------
//            for (int i = 0; i < 10; i++) {
//                Node node2 = new Node();
//                node2.setRenderable(arrowsRenderable);
//                node2.setParent(mAnchorNode);
//                nodes.add(node2);
//            }
            //-------------------

            for (int i = 0; i < 20; i++) {
                Node node2 = new Node();
                node2.setRenderable(arrowsRenderable);
                node2.setParent(mAnchorNode);
                node2.setLocalPosition(new Vector3(0, 0 - .5f,0 - (0.2f * i)));
                node2.setLocalRotation(Quaternion.axisAngle(new Vector3(1f, 0f, 0f), -90f));
                nodes.add(node2);
            }


            //------------------
//            final ValueAnimator animator = ValueAnimator.ofFloat(-1f, 0.1f);
//            animator.setRepeatCount(ValueAnimator.INFINITE);
//            animator.setDuration(1000L);
//            animator.setInterpolator(new LinearInterpolator());
//            animator.addUpdateListener(valueAnimator -> {
//                float value = (float) valueAnimator.getAnimatedValue();
//                float zIndex = 0;
//                for (Node node1 : nodes) {
//                    if (value + zIndex > -0.1f) {
//                        node1.setLocalPosition(new Vector3(0, 0, -1f + value + zIndex));
//                    } else {
//                        node1.setLocalPosition(new Vector3(0, 0, value + zIndex));
//                    }
//                    zIndex = zIndex + 0.1F;
//                }
//            });
//            animator.start();
            //-------------------



            //this snippet of code did not exist from beginning
//           Node node2 = new Node();
//            node2.setRenderable(arrowsRenderable);
//            node2.setLocalPosition(new Vector3(0,0,1));
//            node2.setParent(mAnchorNode);

//            node.setOnTapListener(new Node.OnTapListener() {
//                @Override
//                public void onTap(HitTestResult hitTestResult, MotionEvent motionEvent) {
//                   // showBottomSheet();
//                }
//            });
        }
        Vector3 cameraPos = arFragment.getArSceneView().getScene().getCamera().getWorldPosition();

//        Vector3 forwardLocation = arFragment.getArSceneView().getScene().getCamera().getForward();
        Vector3 camPointerVector = Vector3.subtract(new Vector3(0f,-.10f,-4f), cameraPos);




        if(pointerNode != null) {
            Vector3 camPointerUnitVector = camPointerVector.normalized();
            for (int i = 0; i < nodes.size(); i++) {
                Vector3 nodeLocation = new Vector3(
                        cameraPos.x + i * .2f * camPointerUnitVector.x,
                        -.10f + cameraPos.y + i * .2f * camPointerUnitVector.y,
                        cameraPos.z + i * .2f * camPointerUnitVector.z);
                nodes.get(i).setLocalPosition(nodeLocation);
            }
            pointerNode.setLocalPosition(
                    new Vector3(
                            cameraPos.x + 20 * .2f * camPointerUnitVector.x,
                            -.10f + cameraPos.y + 20 * .2f * camPointerUnitVector.y,
                            cameraPos.z + 20 * .2f * camPointerUnitVector.z
                    ));
        }
    }


    private void moveTarget(int x, int y) {
        targetView.setX(x);
        targetView.setY(y);
    }

    private void hideTarget() {
        targetView.setVisibility(View.GONE);
    }

    private void arFind() {
        if (checkGooglePlayServices()) {
            createLocationRequest();
            checkForLocationSettings();
            getMyLocation();
            startLocationUpdates();
            setUpSensor();

            mockData = new MockData(this);
            mockData.startMockData();
        }
    }

    public boolean checkGooglePlayServices() {
        Log.d(TAG, "checkGooglePlayServices");

        GoogleApiAvailability gApi = GoogleApiAvailability.getInstance();
        int resultCode = gApi.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (gApi.isUserResolvableError(resultCode)) {
                Log.d(TAG, "isUserResolvableError " + resultCode);
                gApi.getErrorDialog(((Activity) this), resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(this, "Google Play Services not Available. Please update your Google Play Services to get your Location", Toast.LENGTH_LONG).show();
                Log.d(TAG, "Google Play Services not Available");

            }
            return false;
        }
        return true;
    }

    public void createLocationRequest() {
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(MIN_UPDATE_INTERVAL);
        locationRequest.setFastestInterval(5000); //in milliseconds
//        locationRequest.setSmallestDisplacement(locationDistance);//1
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    /**
     * Check for location settings.
     */
    public void checkForLocationSettings() {
        try {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
            builder.addLocationRequest(locationRequest);
            SettingsClient settingsClient = LocationServices.getSettingsClient(this);

            settingsClient.checkLocationSettings(builder.build())
                    .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                        @Override
                        public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                            //Setting is success...
                            Toast.makeText(MainActivity.this, "Enabled the Location successfully. ", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            int statusCode = ((ApiException) e).getStatusCode();
                            switch (statusCode) {
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                                    try {
                                        // Show the dialog by calling startResolutionForResult(), and check the
                                        // result in onActivityResult().
                                        ResolvableApiException rae = (ResolvableApiException) e;
                                        rae.startResolutionForResult(MainActivity.this, REQUEST_PERMISSIONS_LOCATION_SETTINGS_REQUEST_CODE);
                                    } catch (IntentSender.SendIntentException sie) {
                                        sie.printStackTrace();
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    Toast.makeText(MainActivity.this, "Setting change is not available.Try in another device.", Toast.LENGTH_LONG).show();
                            }

                        }
                    });

        } catch (Exception ex) {
            ex.printStackTrace();
            Log.e(TAG, "error: " + ex.getMessage());
        }
    }

    @SuppressLint("MissingPermission")
    public void getMyLocation() {
        Log.d(TAG, "getMyLocation");

        try {
            if (!Util.isPermissionsGranted(this)) {

                return;
            }

            mFusedLocationClient.getLastLocation()
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e(TAG, "getMyLocation. Error: " + e.getMessage());

                        }
                    })
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                // Logic to handle location object
                                myLocation = location;
                                String result = "getLastLocation: Current Location Latitude is " +
                                        myLocation.getLatitude() + "\n" +
                                        "Current location Longitude is " + myLocation.getLongitude();
                                Log.d(TAG, "result: " + result);
//                                calculateDistance();

                            } else
                                Log.d(TAG, "location is null in addOnSuccessListener");

                        }
                    });


        } catch (Exception ex) {
            ex.printStackTrace();
            Log.e(TAG, "error: " + ex.getMessage());

        }
    }

    @SuppressLint("MissingPermission")
    public void startLocationUpdates() {
        Log.d(TAG, "startLocationUpdates");
        try {
            if (!Util.isPermissionsGranted(this)) {
                return;
            }
            mFusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, null/*Looper.myLooper()*/);

        } catch (Exception ex) {
            ex.printStackTrace();
            Log.e(TAG, "error: " + ex.getMessage());
        }
    }

    private void setUpSensor() {
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senMagnet = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        sensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, senMagnet, SensorManager.SENSOR_DELAY_NORMAL);


    }


    private void calculateDistance(TrackedLocationObject locationObject) {
        this.locationObject = locationObject;
        Log.d(TAG, "calculateDistance");
        if (myLocation != null && locationObject != null) {
            float[] distance = new float[3];
            Location.distanceBetween(myLocation.getLatitude(), myLocation.getLongitude(), locationObject.getLat(), locationObject.getLng(), distance);
            mDistance = distance[0];
            mInitialBearing = distance[1];
            mFinalBearing = distance[2];

            double targetHeading = heading;

            double compassDirection = mInitialBearing < 0 ? mInitialBearing + 360 : mInitialBearing;
            double adjustment = (targetHeading - compassDirection) * -1;
            if (adjustment > 180) {
                adjustment = adjustment - 360;
            } else if (adjustment < -180) {
                adjustment = adjustment + 360;
            }
            boolean isLeft;
            if (adjustment > 0) {
                isLeft = false;
            } else {
                isLeft = true;
            }
            double degrees = adjustment > 0 ? adjustment : (adjustment * -1);
            animateArrows(degrees, isLeft);
        }
    }


    private synchronized void animateArrows(double degrees, boolean isLeft) {
        long curTime = System.currentTimeMillis();
        if ((curTime - lastArrowAnimated) > 800) {
            lastArrowAnimated = curTime;
            showDots();
            if (Math.abs(degrees) < (ARConfig.TARGET_INDICATOR_WIDGET_BEARING_RING_THRESHOLD_A_TARGET)) {
                showTarget();
            } else {
                if (isLeft) {
                    if (degrees > ARConfig.TARGET_INDICATOR_WIDGET_BEARING_RING_THRESHOLD_A && degrees < ARConfig
                            .TARGET_INDICATOR_WIDGET_BEARING_RING_THRESHOLD_B) {
                        animateLeftArrows(1);
                    } else if (degrees > ARConfig.TARGET_INDICATOR_WIDGET_BEARING_RING_THRESHOLD_B && degrees < ARConfig.TARGET_INDICATOR_WIDGET_BEARING_RING_THRESHOLD_C) {
                        animateLeftArrows(3);
                    } else if (degrees > ARConfig.TARGET_INDICATOR_WIDGET_BEARING_RING_THRESHOLD_C) {
                        animateLeftArrows(5);
                    }
                } else {
                    if (degrees > ARConfig.TARGET_INDICATOR_WIDGET_BEARING_RING_THRESHOLD_A && degrees < ARConfig
                            .TARGET_INDICATOR_WIDGET_BEARING_RING_THRESHOLD_B) {
                        animateRightArrows(1);
                    } else if (degrees > ARConfig.TARGET_INDICATOR_WIDGET_BEARING_RING_THRESHOLD_B && degrees < ARConfig.TARGET_INDICATOR_WIDGET_BEARING_RING_THRESHOLD_C) {
                        animateRightArrows(3);
                    } else if (degrees > ARConfig.TARGET_INDICATOR_WIDGET_BEARING_RING_THRESHOLD_C) {
                        animateRightArrows(5);
                    }
                }
            }
        } else {
            Log.d(TAG, "myLocation is null!");
        }
    }

    //}

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    public void OnNewTargetLocationUpdate(TrackedLocationObject locationObject) {
        this.locationObject = locationObject;
        Log.d(TAG, "OnNewTargetLocationUpdate: " + locationObject.getLat() + ", " + locationObject.getLng());
        calculateDistance(locationObject);
        if (locationObject.getHeading() != null && locationObject.getLat() != null && locationObject.getLng() != null) {
            updateSprite(locationObject.getHeading(), locationObject.getLat(), locationObject.getLng());
        }
    }

    void onMyLocationUpdated(Location location) {
        Log.d(TAG, "onMyLocationUpdated: ");

        myLocation = location;
        calculateDistance(locationObject);

        if (!camSetLocation) {
            moveCamToMyLocation();
            camSetLocation = true;
        }
    }

    private void moveCamToMyLocation() {
//        let newCam = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
//                longitude: location.coordinate.longitude,
//                zoom: 16)
    }

    private void updateSprite(Double heading, Double lat, Double lng) {

    }

    // region SensorEventListener
    float[] gravity = new float[3];
    float[] geomag = new float[3];
    double lastHeadingValue;

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        Sensor mySensor = sensorEvent.sensor;

        boolean ready = false;
        switch (sensorEvent.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                float x = sensorEvent.values[0];
                float y = sensorEvent.values[1];
                float z = sensorEvent.values[2];
                gravity[0] = x;
                gravity[1] = y;
                gravity[2] = z;

                Log.d(TAG, "x: " + x + ", y:" + y + ", z" + z);
                // float speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000;
                //if (speed > SHAKE_THRESHOLD) {

                //}

                if (geomag[0] != 0)
                    ready = true;
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                geomag[0] = sensorEvent.values[0];
                geomag[1] = sensorEvent.values[1];
                geomag[2] = sensorEvent.values[2];
                if (gravity[2] != 0)
                    ready = true;

                break;
        }
        if (!ready)
            return;
        float[] rotationMatrix = new float[16];
        float[] vals = new float[16];
        boolean cek = SensorManager.getRotationMatrix(rotationMatrix, null, gravity, geomag);

        if (cek) {
            SensorManager.getOrientation(rotationMatrix, vals);
            heading = vals[0] * (180 / Math.PI);

//            if (showTarget) {
//                double adjustment = heading * -1;
//                if (adjustment > 180) {
//                    adjustment = adjustment - 360;
//                } else if (adjustment < -180) {
//                    adjustment = adjustment + 360;
//                }
//                double x = adjustment > 0 ? adjustment : (adjustment * -1);
//                int direction = 1;
//                int initial = 0;
//                if (adjustment < 0) {
//                    direction *= -1;
//                    x /= 3;
//                    initial = -500;
//                }
//                long curTime = System.currentTimeMillis();
//
//                if ((curTime - lastUpdate) > 100) {
//                    Log.d(TAG, "onSensorChanged");
//                    moveTarget(initial + (direction * (int) x * 8), (int) ((gravity[1] * 110) - 700));
//                    showTarget();
//                    lastUpdate = curTime;
//                    last_degree = x;
//                }
//            } else {
//                hideTarget();
//            }
//
            long curTime = System.currentTimeMillis();
            if ((curTime - lastHeading) > 600) {
                lastHeading = curTime;
                calculateDistance(locationObject);
            }
            // calculateDistance(locationObject, AnimationType.TARGET);
//
//            if ((curTime - lastTarget) > 1000) {
//                lastTarget = curTime;
//                calculateDistance(locationObject, AnimationType.TARGET);
//            }
            //  targetView.setY((int) ((gravity[1] * 110) - 700));
            double changeX = Math.abs(Math.abs(gravity[0] - Math.abs(last_x))) * 1000;
            if (changeX > 3) {
                //  if (Math.abs(Math.abs(heading)-Math.abs(lastHeadingValue)) >3) {
                //   if (Math.abs(Math.abs(gravity[1]) - Math.abs(last_y)) > 3)
                lastHeadingValue = heading;
                last_x = gravity[0];
                last_y = gravity[1];
                last_z = gravity[2];
            }
        }
    }

    double heading;

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    //endregion

    public static boolean checkIsSupportedDeviceOrFinish(final Activity activity) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            Log.e(TAG, "Sceneform requires Android N or later");
            Toast.makeText(activity, "Sceneform requires Android N or later", Toast.LENGTH_LONG).show();
            activity.finish();
            return false;
        }
        String openGlVersionString =
                ((ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE))
                        .getDeviceConfigurationInfo()
                        .getGlEsVersion();
        if (Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {
            Log.e(TAG, "Sceneform requires OpenGL ES 3.0 later");
            Toast.makeText(activity, "Sceneform requires OpenGL ES 3.0 or later", Toast.LENGTH_LONG)
                    .show();
            activity.finish();
            return false;
        }
        return true;
    }
}