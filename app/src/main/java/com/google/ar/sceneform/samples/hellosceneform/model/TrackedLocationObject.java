package com.google.ar.sceneform.samples.hellosceneform.model;

public class TrackedLocationObject {

    Double lat;
    Double lng;
    double alt;
    Double heading = 180.0;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double getHeading() {
        return heading;
    }

    public void setHeading(Double heading) {
        this.heading = heading;
    }


    public TrackedLocationObject(Double lat, Double lng, Double heading) {
        this.lat = lat;
        this.lng = lng;
        this.heading = heading;
    }

    public TrackedLocationObject(Double lat, Double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public Double getAlt() {
        return alt;
    }

    public void setAlt(double alt) {
        this.alt = alt;
    }
}
