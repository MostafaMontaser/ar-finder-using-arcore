package com.google.ar.sceneform.samples.hellosceneform.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class Util {

    private static final String TAG = Util.class.getSimpleName();
    public static final int PERMISSIONS_REQUEST_CODE = 99;

    public static Double getAngleBetween(PointF point1, PointF point2){

        double p1x = degreesToRadians(point1.x);
        double p1y = degreesToRadians(point1.y);

        double p2x = degreesToRadians(point2.x);
        double p2y = degreesToRadians(point2.y);

        double dLon = p1y - p2y;

        double y = sin(dLon) * cos(p2x);
        double x = cos(p1x) * sin(p2x) - sin(p1x) * cos(p2x) * cos(dLon);
        double radiansBearing = atan2(y, x);

        return radiansToDegrees(radiansBearing);
    }


    public static Double degreesToRadians(double degrees) { return degrees * Math.PI / 180.0; } // Math.toRadians( 180 )
    public static Double radiansToDegrees(double radians) { return radians * 180.0 / Math.PI; } // Math.toDegrees( Math.PI )
    public static Double getDist(PointF point1, PointF point2){
        double deltaX = point2.x - point1.x;
        double deltaY = point2.y - point1.y;
        return  Math.sqrt( (deltaX * deltaX) + (deltaY * deltaY) );
    }
    public static Double getDist(Location loc1, Location loc2){
        double deltaX = loc2.getLatitude() - loc1.getLatitude();
        double deltaY = loc2.getLongitude() - loc1.getLongitude();
        return  Math.sqrt( (deltaX * deltaX) + (deltaY * deltaY) );
    }

    public static Double getDist(Double lat1, Double long1, Double lat2, Double long2){
        double deltaX = lat2 - lat1;
        double deltaY = long2 - long1;
        return  Math.sqrt( (deltaX * deltaX) + (deltaY * deltaY) );
    }

    //region Permissions

    /**
     * Check Permissions
     *
     * @param context
     * @return
     */
    public static boolean isPermissionsGranted(Context context) {
        Log.d(TAG, "isPermissionsGranted");

        int accessFineLocation = ContextCompat.checkSelfPermission(context.getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION);
        int accessLocation = ContextCompat.checkSelfPermission(context.getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int camera = ContextCompat.checkSelfPermission(context.getApplicationContext(), android.Manifest.permission.CAMERA);

        return (accessFineLocation == PackageManager.PERMISSION_GRANTED)
                && (accessLocation == PackageManager.PERMISSION_GRANTED)
                && (camera == PackageManager.PERMISSION_GRANTED);

    }

    /**
     * Request Permissions
     *
     * @param context
     * @return
     */
    public static void requestPermissions(Activity context) {

        Log.d(TAG, "requestPermissions");

        int accessFineLocation = ContextCompat.checkSelfPermission(context.getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION);
        int accessLocation = ContextCompat.checkSelfPermission(context.getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION);

        int camera = ContextCompat.checkSelfPermission(context.getApplicationContext(), android.Manifest.permission.CAMERA);

        List<String> permissionsNeeded = new ArrayList<String>();

        if(accessFineLocation != PackageManager.PERMISSION_GRANTED)
            permissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        if(accessLocation != PackageManager.PERMISSION_GRANTED)
            permissionsNeeded.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);

        if(camera != PackageManager.PERMISSION_GRANTED)
            permissionsNeeded.add(android.Manifest.permission.CAMERA);

        if(permissionsNeeded.size()>0) {

            Log.d(TAG, "ASK...permissionsNeeded.size() = " + permissionsNeeded.size());
            ActivityCompat.requestPermissions(context, permissionsNeeded.toArray(new String[permissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
        }

    }



    //endregion

}
