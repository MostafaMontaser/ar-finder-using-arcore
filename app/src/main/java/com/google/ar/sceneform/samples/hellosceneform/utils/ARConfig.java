package com.google.ar.sceneform.samples.hellosceneform.utils;

public class ARConfig {
    public static final double TARGET_INDICATOR_WIDGET_BEARING_RING_THRESHOLD_A = 15;
    public static final double TARGET_INDICATOR_WIDGET_BEARING_RING_THRESHOLD_B = 70;
    public static final double TARGET_INDICATOR_WIDGET_BEARING_RING_THRESHOLD_C = 120;
    public static final double TARGET_INDICATOR_WIDGET_BEARING_RING_THRESHOLD_A_TARGET = 30;


}
